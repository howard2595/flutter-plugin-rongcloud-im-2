//
//  LATipsMessage.m
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import "LATipsMessage.h"

@implementation LATipsMessage

+ (NSString *)getObjectName {
    return @"LA:TipsMsg";
}

+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_NONE;
}

@end
