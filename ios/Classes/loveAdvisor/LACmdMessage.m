//
//  LACmdMessage.m
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import "LACmdMessage.h"

@implementation LACmdMessage

+ (NSString *)getObjectName {
    return @"LA:CmdMsg";
}

+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISPERSISTED;
}

@end
