#import "NSString+WPFlutter.h"

@implementation NSString (WPFlutter)
- (BOOL)wpf_isEmpty{
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
    
}

- (NSString *)wpf_getStringUppercaseInRange:(NSRange)range{
    NSString *result = [self substringWithRange:range];
    result = [self stringByReplacingCharactersInRange:range withString:[result uppercaseString]];
    return result;
}

@end
