#import "WPFRCStatusMsg.h"
@class WPFUserInfo;

@interface WPFRCMatchNtf : WPFRCStatusMsg
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSArray<NSNumber *> *interestIds;
@property (nonatomic, strong) WPFUserInfo *user;
@end
