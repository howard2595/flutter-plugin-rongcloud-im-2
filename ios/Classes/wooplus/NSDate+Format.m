#import "NSDate+Format.h"

@implementation NSDate (Format)
- (NSString *)wpf_getStringFromDateWithFormat:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    @try {
        return [formatter stringFromDate:self];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

@end
