#import <Foundation/Foundation.h>

@interface WPCmdMsgContent : NSObject

@property (nonatomic, strong) NSDate *createdAt;

@property (nonatomic, assign) NSInteger age;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, strong) NSArray<NSNumber *> *interest;

@property (nonatomic, copy) NSString *reason;

+(instancetype)contentWithDataString:(NSString *)dataString;
@end
