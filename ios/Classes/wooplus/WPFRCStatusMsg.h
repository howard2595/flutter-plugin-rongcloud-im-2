//
//  WPFRCStatusMsg.h
//  WooPlus
//
//  Created by TeamN on 16/03/2017.
//  Copyright © 2017 TeamN. All rights reserved.
//

#import <RongIMLibCore/RongIMLibCore.h>


@interface WPFRCStatusMsg : RCStatusMessage
@property (nonatomic, strong) NSString *extra;
- (NSMutableDictionary *)dictionayForEncode;
@end
