//
//  LATextMessage.m
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import "LATextMessage.h"

@implementation LATextMessage

+ (NSString *)getObjectName {
    return @"LA:TxtMsg";
}


+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_ISCOUNTED;
}

@end
