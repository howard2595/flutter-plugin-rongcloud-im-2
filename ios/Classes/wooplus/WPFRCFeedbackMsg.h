#import "WPFRCMsgContent.h"

@interface WPFRCFeedbackMsg : WPFRCMsgContent
@property (nonatomic, strong) NSString *feedbackId;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, assign) NSInteger type;
@end
