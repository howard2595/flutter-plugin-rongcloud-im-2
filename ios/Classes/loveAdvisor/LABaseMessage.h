//
//  LABaseMessage.h
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import <RongIMLibCore/RongIMLibCore.h>

NS_ASSUME_NONNULL_BEGIN

/**
    占卜师业务 基础消息类型
 */
@interface LABaseMessage : RCMessageContent

@property (nonatomic, copy) NSString *content;

@end

NS_ASSUME_NONNULL_END
