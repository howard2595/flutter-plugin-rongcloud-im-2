#import "WPFRCMsgContent.h"

@interface WPFRCGiftOpenNtf : WPFRCMsgContent
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *giftId;
@property (nonatomic, strong) NSString *giftName;
@property (nonatomic, readonly) NSAttributedString *attributedString;
@end
