#import <Foundation/Foundation.h>
#import "WPCmdMsgContent.h"

typedef NS_ENUM(NSInteger, WPCommandMessageType) {
    WPFCmdMessageTypeUnMatch     = 0,
    WPFCmdMessageTypeMatch       = 1,
    WPFCmdMessageTypeRematch     = 2,
    WPFCmdMessageTypeVIPStart    = 3,
    WPFCmdMessageTypeUserKillOut = 4,
    WPFCmdMessageTypeExtend      = 5,
};

@interface WPCmdMessage : NSObject

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) WPCommandMessageType commandType;

@property (nonatomic, copy) NSString *targetId;

@property (nonatomic, copy) NSString *sendAt;

@property (nonatomic, assign) NSInteger direction;

@property (nonatomic, strong) WPCmdMsgContent *content;

+(instancetype)contentWithDataString:(NSString *)dataString;

@end
