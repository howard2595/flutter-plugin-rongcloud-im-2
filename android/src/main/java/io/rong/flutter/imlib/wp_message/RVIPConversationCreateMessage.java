package io.rong.flutter.imlib.wp_message;

import android.os.Parcel;

import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:VIPConversationStartCommand", flag = MessageTag.NONE)
public class RVIPConversationCreateMessage extends InformationNotificationMessage {
    public RVIPConversationCreateMessage() {}
    
    public RVIPConversationCreateMessage(Parcel in) {
      super(in);
    }
    
    public RVIPConversationCreateMessage(byte[] data) {
        super(data);
//        String jsonStr = null;
//        try {
//            jsonStr = new String(data, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            RLog.e("JSONException", e.getMessage());
//        }
//        RVIPConversationCreateMessage rvipConversationCreateMessage = JSONObject.parseObject(jsonStr, RVIPConversationCreateMessage.class);
//        setMessage(rvipConversationCreateMessage.getMessage());
//        setExtra(rvipConversationCreateMessage.getExtra());
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public byte[] encode() {
        return super.encode();
//        JSONObject jsonObj = new JSONObject();
//        try {
//            jsonObj.put("message", getMessage());
//            if (!TextUtils.isEmpty(getExtra()))
//                jsonObj.put("extra", getExtra());
//        } catch (JSONException e) {
//            RLog.e("JSONException", e.getMessage());
//        }
//        try {
//            return jsonObj.toString().getBytes("UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return null;
    }
    

    public static final Creator<RVIPConversationCreateMessage> CREATOR = new Creator<RVIPConversationCreateMessage>() {
        @Override
        public RVIPConversationCreateMessage createFromParcel(Parcel source) {
            return new RVIPConversationCreateMessage(source);
        }
        @Override
        public RVIPConversationCreateMessage[] newArray(int size) {
            return new RVIPConversationCreateMessage[size];
        }
    };
    
}
