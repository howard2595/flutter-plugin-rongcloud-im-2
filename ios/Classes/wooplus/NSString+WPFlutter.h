#import <Foundation/Foundation.h>

@interface NSString (WPFlutter)
- (BOOL)wpf_isEmpty;
- (NSString *)wpf_getStringUppercaseInRange:(NSRange)range;
@end
