#import "WPFRCStatusMsg.h"
@class WPFUserInfo;

@interface WPFRCStartVIPConversationCmd : WPFRCStatusMsg
@property (nonatomic, strong) WPFUserInfo *user;
@end
